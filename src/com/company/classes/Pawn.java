package com.company.classes;

import java.awt.*;

public class Pawn extends Piece{

    public Pawn(boolean color, Point position){

        super(color, position, "pawn.png", new Point[]{new Point(0, 1)}, 2);
    }
}
