package com.company.classes;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Piece {

    private boolean color;
    public Point positon;
    public BufferedImage pieceImage;
    public Point[] moves;
    public int repeatMove;
    public boolean alreadyMoved;

    public Piece(boolean color, Point positon, String pieceImage, Point[] moves, int repeatMove){
        this.color =color;
        this.positon = positon;
        this.moves = moves;
        this.repeatMove = repeatMove;
        this.alreadyMoved = false;

        try {
            if(color) {
                this.pieceImage = ImageIO.read(new File("images/w"+pieceImage));
            }
            else {
                this.pieceImage = ImageIO.read(new File("images/b"+pieceImage));
            }
        } catch (IOException e) {
            System.out.println("File not found: " + e.getMessage());
        }
    }

    public boolean isWhite(){
        return color;
    }


}
